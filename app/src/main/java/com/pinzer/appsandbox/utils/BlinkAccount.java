package com.pinzer.appsandbox.utils;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Context;
import android.os.Build;
import android.util.Log;
import android.widget.Toast;

import com.pinzer.appsandbox.BlinkApp;
import com.pinzer.appsandbox.BuildConfig;

/**
 * Created by pinzer on 7/25/2017.
 */

public class BlinkAccount {
    private static final String TAG = BlinkAccount.class.getSimpleName();

    public static final String BLINK_ACCOUNT = BuildConfig.APPLICATION_ID;



    public static Account addBlinkAccount(String username, String password,  Context context) {
        Account account = BlinkAccountService.getAccount(username, BLINK_ACCOUNT);
        AccountManager accountManager = (AccountManager) context.getSystemService(Context.ACCOUNT_SERVICE);

        try {
            password = BlinkApp.getApp().keystoreManager.encryptText(password);
        } catch (BlinkKeystoreManagerException e) {
            Toast.makeText(context, e.getMessage(), Toast.LENGTH_SHORT).show();
        }

        if(accountManager.addAccountExplicitly(account, password, null)) {
            Toast.makeText(context, "Added an account", Toast.LENGTH_SHORT).show();
        } else {
            accountManager.setPassword(account, password);
            Toast.makeText(context, "Updated Password", Toast.LENGTH_SHORT).show();
        }
        return account;
    }

    public static void removeBlinkAccount(Account account, Context context) {
        if(account != null) {
            AccountManager accountManager = (AccountManager) context.getSystemService(Context.ACCOUNT_SERVICE);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
                accountManager.removeAccount(account, null, null, null);
            } else {
                //noinspection deprecation
                accountManager.removeAccount(account, null, null);
            }
            Log.d(TAG, "Removed account: " + account.name);
        }
    }

    public static String getPassword(Account account, Context context) {
        if(account != null) {
            AccountManager accountManager = (AccountManager) context.getSystemService(Context.ACCOUNT_SERVICE);
            try {
                return BlinkApp.getApp().keystoreManager.decryptText(accountManager.getPassword(account));
            } catch (BlinkKeystoreManagerException e) {
                e.printStackTrace();
                Log.d(TAG, "Couldn't Decrypt the password");
            }
        }
        return null;
    }

    public static Account getBlinkAccount(Context context) {
        AccountManager accountManager = (AccountManager) context.getSystemService(Context.ACCOUNT_SERVICE);
        // "this" references the current Context

        Account[] accounts = accountManager.getAccountsByType(BLINK_ACCOUNT);
        if(accounts.length > 0) {
            return accounts[0];
        }
        Log.d(TAG, "No accounts available");
        return null;
    }
}
