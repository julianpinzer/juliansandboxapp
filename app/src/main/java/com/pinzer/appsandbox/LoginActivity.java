package com.pinzer.appsandbox;

import android.accounts.Account;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.pinzer.appsandbox.demo.SimulateCredentials;
import com.pinzer.appsandbox.utils.BlinkAccount;

/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends AppCompatActivity {

    private static final String TAG = LoginActivity.class.getSimpleName();

    private EditText usernameEditText;
    private EditText passwordEditText;
    private Button loginButton;

    private String username = "";
    private String password = "";
    private Account account;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_login);

        usernameEditText = (EditText) findViewById(R.id.email);
        passwordEditText = (EditText) findViewById(R.id.password);

        account = BlinkAccount.getBlinkAccount(this);
        if (account != null) {
            username = account.name;
            password = BlinkAccount.getPassword(account, this);
            if(!password.isEmpty()) {
                login();
            }

        }

        if(username.isEmpty()) {
            username = BlinkApp.getApp().getSPUsername();
        }
        loginButton = (Button) findViewById(R.id.email_sign_in_button);
        usernameEditText.setText(username);

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                login();
            }
        });

    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.activity_login_menu, menu);
        return true;    
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.login_menu:
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }




    private void login() {
        if(!usernameEditText.getText().toString().isEmpty() && !passwordEditText.getText().toString().isEmpty()) {
            username = usernameEditText.getText().toString();
            password = passwordEditText.getText().toString();
        }
        if(SimulateCredentials.INSTANCE.login(username, password)) {
            if(!usernameEditText.getText().toString().isEmpty() && !passwordEditText.getText().toString().isEmpty()) {
                BlinkAccount.addBlinkAccount(username, password, this);
            }
            Log.d(TAG, username + " " + password);

            BlinkApp.getApp().setSPUsername(username);

            Intent i = new Intent(this, HomescreenActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(i);
        } else {
            usernameEditText.setError("Invalid Credentials!");
        }

    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }
}

