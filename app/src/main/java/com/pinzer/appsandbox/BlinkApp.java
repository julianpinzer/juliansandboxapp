package com.pinzer.appsandbox;

import android.app.Application;
import android.content.SharedPreferences;
import android.widget.Toast;

import com.facebook.stetho.Stetho;
import com.pinzer.appsandbox.utils.BlinkKeystoreManager;
import com.pinzer.appsandbox.utils.BlinkKeystoreManagerException;

import static android.preference.PreferenceManager.getDefaultSharedPreferences;

/**
 * Created by pinzer on 7/25/2017.
 */

public class BlinkApp extends Application{

    private static final String PREF_USERNAME = "blink_username";
    private static final String PREF_IV = "blink_iv";
    private static BlinkApp app;
    public BlinkKeystoreManager keystoreManager;

    @Override
    public void onCreate() {
        app = this;
        Stetho.initializeWithDefaults(this);
        try {
            keystoreManager = new BlinkKeystoreManager(this);
        } catch (BlinkKeystoreManagerException e) {
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
        }
        super.onCreate();
    }

    static public BlinkApp getApp() {
        return app;
    }

    public String getSPUsername(){
        SharedPreferences sp = getDefaultSharedPreferences(this);
        return sp.getString(PREF_USERNAME, "");
    }

    public void setSPUsername(String username) {
        SharedPreferences sp = getDefaultSharedPreferences(this);
        sp.edit().putString(PREF_USERNAME, username).apply();
    }
}
